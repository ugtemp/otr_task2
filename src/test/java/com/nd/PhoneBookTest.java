package com.nd;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Shishkov A.V. on 02.02.19.
 */
public class PhoneBookTest {
	private PhoneBook book = new PhoneBook();

	@Before
	public void setUp() throws Exception {
		book.loadAbonentsFrom("abonents.json");
	}

	@Test(expected = ClassNotFoundException.class)
	public void loadNotExistedFile() throws Exception {
		book.loadAbonentsFrom("dummy.file");
	}

	@Test
	public void ivanovIsFound() {
		assertNotNull("Ошибка поиска в БД Абоненты: не найден абонент Иванов И.И.",
				book.findPhonesByName("Иванов И.И."));
	}

	@Test
	public void dummyAbonentNotFound() {
		assertNull(book.findPhonesByName("путин в.в."));
	}
}