package com.nd;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Shishkov A.V. on 01.02.19.
 */
public class Application {
	private static final PhoneBook book = new PhoneBook();

	private static void initializeDb() {
		try {
			book.loadAbonentsFrom("abonents.json");
		} catch (IOException e) {
			System.out.println("Ошибка загрузки файла: нет такого файла (abonent.json)");
			System.exit(-1);
		} catch (ParseException e) {
			System.out.println("Ошибка чтения файла абонентов: неверный формат");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		}
	}

	public static void main(String[] args) {
		initializeDb();

		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Введите ФИО:");
			String name = scanner.nextLine();

			List<String> phones = book.findPhonesByName(name);

			if (phones == null) {
				System.out.println("БД Абонентов пуста!");
				System.exit(0);
			}

			if (phones.isEmpty()) {
				System.out.println("В БД нет абонента с заданным именем: " + name);
				System.exit(0);
			}

			int index = 1;
			for (String phone : phones) {
				System.out.printf("%d. %s\n", index++, phone);
			}
		}
	}
}
