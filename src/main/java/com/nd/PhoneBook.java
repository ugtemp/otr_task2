package com.nd;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Shishkov A.V. on 01.02.19.
 */
public class PhoneBook {
	private final Map<String, ArrayList<String>> records = new HashMap<String, ArrayList<String>>();

	private Object readJsonFromFile(String filename) throws IOException, ParseException, ClassNotFoundException {
		try {
			if (filename == null || filename.isEmpty())
				throw new ClassNotFoundException("Ошибка чтения файла: файл " + filename + " не найден");

			File file = new File(Objects.requireNonNull(getClass().getClassLoader().getResource(filename)).getFile());
			FileReader reader = new FileReader(file);
			JSONParser parser = new JSONParser();
			return parser.parse(reader);
		} catch (NullPointerException e) {
			throw new ClassNotFoundException("Ошибка чтения файла: файл " + filename + " не " +
					"найден");
		}
	}

	public void loadAbonentsFrom(String fileName) throws Exception {
		try {
			JSONObject json = (JSONObject) readJsonFromFile(fileName);
			records.clear();

			for (Object key : json.keySet()) {
				JSONArray values = (JSONArray) json.get(key);
				records.put(((String) key).trim().toLowerCase(), values);
			}
		} catch (ClassCastException e) {
			throw new Exception("Ошибка чтения файла: неверный формат данных в файле");
		}
	}

	public ArrayList<String> findPhonesByName(String name) {
		return records.get(name.trim().toLowerCase());
	}
}
